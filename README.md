# react-pe-admin-module

 [Модуль](https://protopiahome-public.gitlab.io/protopia-ecosystem/cra-template-pe/modules/) для [ProtopiaEcosystem React Client](https://protopiahome-public.gitlab.io/protopia-ecosystem/cra-template-pe/). Включает в себя набор Экранов, Виджетов, настроек и ассетов для генерации Панели Администрирования приложения.


## Подробное описание:  

Модуль расширяет web-клиент ProtopiaEcosystem функциональностью визуальной админской панели.

В любом приложении множество Типов Данных. Есть посты, Статьи, События, Места, Карты, Организации, Товары, Пользователи ets.

Каждый дополнительный модуль расширяет возможности Вашего приложения, но добавляет новые Типы Данных и потребность в их модерировании и контроле.

Модуль PE-Admin после установки и конфигурации позволяет быстро и надёжно оперировать Вашими данными путём несложных манипуляций.

[см.](https://protopiahome-public.gitlab.io/protopia-ecosystem/cra-template-pe/admin-module/)

## Установка

1. В корневой папке приложения запускаем

``` 
npm run add-module pe-admin-module https://protopiahome-public.gitlab.io/protopia-ecosystem/cra-template-pe/react-pe-admin-module
```

 2. Запускаем локальную сессию:

``` 
npm start
```

 или компиллируем приложение для последующего размещения на сервере:

``` 
npm run build
```

 ## Структура и сценарии использования:

 [см.](https://protopiahome-public.gitlab.io/protopia-ecosystem/cra-template-pe/admin-module-views/)