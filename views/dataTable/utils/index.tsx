export const getTableRows = (fields: any) => {
    const apollo_fields: any = fields.apollo_fields
    return Object.keys(apollo_fields).filter( ( af: string ) => {
        return apollo_fields[af].thread 
            && !apollo_fields[af].hidden
            && !apollo_fields[af].field_footer
    } )
}