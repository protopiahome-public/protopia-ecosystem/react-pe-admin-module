import HTMLSourceVew from "@/modules/pe-basic-module/views/HTMLSourceVew"

const DefaultWidget = () : JSX.Element => {
  return <HTMLSourceVew
    html_source="/assets/data/admin/widgets.default.html"
    hide_title
  />
}

export default DefaultWidget